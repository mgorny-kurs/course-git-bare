package pl.net.divo.course;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.net.divo.course.model.Photo;
//import pl.net.divo.course.repository.PhotoRepository;

import java.util.List;

class PhotoControllerTest extends AbstractIntegrationTest {
//    @Autowired
//    PhotoRepository photoRepository;
//
//    @Test
//    void shouldNotFindAnyPhotos() {
//        // when
//        List<Photo> photos = doGet("/tests", new TypeReference<List<Photo>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, photos.size());
//    }
//
//    @Test
//    void shouldAddTestWithoutErrors() {
//        // given
//        Photo photo = new Photo("1", "name", "tester");
//
//        // when
//        doPost("/tests", photo);
//
//        // then
//        Assertions.assertEquals(1, photoRepository.findAll().size());
//        Assertions.assertEquals("name", photoRepository.findById("1").getName());
//        Assertions.assertEquals("tester", photoRepository.findById("1").getTester());
//    }
//
//    @Test
//    void shouldFindAllPhotos() {
//        // given
//        doPost("/tests", new Photo("1", "name1", "tester1"));
//        doPost("/tests", new Photo("2", "name2", "tester2"));
//
//        // when
//        List<Photo> photos = doGet("/tests", new TypeReference<List<Photo>>() {
//        });
//
//        // then
//        Assertions.assertEquals(2, photos.size());
//        Assertions.assertEquals("name1", photos.get(0).getName());
//        Assertions.assertEquals("name2", photos.get(1).getName());
//    }
//
//    @Test
//    void shouldFindById() {
//        // given
//        doPost("/tests", new Photo("1", "name1", "tester1"));
//
//        // when
//        Photo photo = doGet("/tests/1", Photo.class);
//
//        // then
//        Assertions.assertEquals("1", photo.getId());
//        Assertions.assertEquals("name1", photo.getName());
//        Assertions.assertEquals("tester1", photo.getTester());
//    }
//
//    @Test
//    void shouldFindByName() {
//        // given
//        doPost("/tests", new Photo("1", "name1", "tester1"));
//
//        // when
//        List<Photo> photo = doGet("/tests?byName=name1", new TypeReference<List<Photo>>() {
//        });
//
//        // then
//        Assertions.assertEquals(1, photo.size());
//        Assertions.assertEquals("1", photo.get(0).getId());
//        Assertions.assertEquals("name1", photo.get(0).getName());
//        Assertions.assertEquals("tester1", photo.get(0).getTester());
//    }
//
//    @Test
//    void shouldNotFindByNameWhenNotExist() {
//        // given
//        doPost("/tests", new Photo("1", "name1", "tester1"));
//
//        // when
//        List<Photo> photo = doGet("/tests?byName=name2", new TypeReference<List<Photo>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, photo.size());
//    }
//
//    @Test
//    void shouldDeletePhoto() {
//        // given
//        doPost("/tests", new Photo("1", "name1", "tester1"));
//        List<Photo> originalPhotos = photoRepository.findAll();
//
//        // when
//        doDelete("/tests/1");
//        List<Photo> photoAfterDelete = photoRepository.findAll();
//
//        // then
//        Assertions.assertEquals(1, originalPhotos.size());
//        Assertions.assertEquals("name1", originalPhotos.get(0).getName());
//        Assertions.assertEquals(0, photoAfterDelete.size());
//    }
//
//    @AfterEach
//    void clean() {
//        photoRepository.deleteAll();
//    }
}
