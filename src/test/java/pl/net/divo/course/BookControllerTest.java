package pl.net.divo.course;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pl.net.divo.course.model.Book;
//import pl.net.divo.course.repository.BookRepository;

import java.util.List;

class BookControllerTest extends AbstractIntegrationTest {
//    @Autowired
//    BookRepository bookRepository;
//
//    @Test
//    void shouldNotFindAnyBooks() {
//        // when
//        List<Book> books = doGet("/books", new TypeReference<List<Book>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, books.size());
//    }
//
//    @Test
//    void shouldAddTestWithoutErrors() {
//        // given
//        Book book = new Book("1", "name", "tester");
//
//        // when
//        doPost("/books", book);
//
//        // then
//        Assertions.assertEquals(1, bookRepository.findAll().size());
//        Assertions.assertEquals("name", bookRepository.findById("1").getName());
//        Assertions.assertEquals("tester", bookRepository.findById("1").getTester());
//    }
//
//    @Test
//    void shouldFindAllBooks() {
//        // given
//        doPost("/books", new Book("1", "name1", "tester1"));
//        doPost("/books", new Book("2", "name2", "tester2"));
//
//        // when
//        List<Book> books = doGet("/books", new TypeReference<List<Book>>() {
//        });
//
//        // then
//        Assertions.assertEquals(2, books.size());
//        Assertions.assertEquals("name1", books.get(0).getName());
//        Assertions.assertEquals("name2", books.get(1).getName());
//    }
//
//    @Test
//    void shouldFindById() {
//        // given
//        doPost("/books", new Book("1", "name1", "tester1"));
//
//        // when
//        Book book = doGet("/books/1", Book.class);
//
//        // then
//        Assertions.assertEquals("1", book.getId());
//        Assertions.assertEquals("name1", book.getName());
//        Assertions.assertEquals("tester1", book.getTester());
//    }
//
//    @Test
//    void shouldFindByName() {
//        // given
//        doPost("/books", new Book("1", "name1", "tester1"));
//
//        // when
//        List<Book> book = doGet("/books?byName=name1", new TypeReference<List<Book>>() {
//        });
//
//        // then
//        Assertions.assertEquals(1, book.size());
//        Assertions.assertEquals("1", book.get(0).getId());
//        Assertions.assertEquals("name1", book.get(0).getName());
//        Assertions.assertEquals("tester1", book.get(0).getTester());
//    }
//
//    @Test
//    void shouldNotFindByNameWhenNotExist() {
//        // given
//        doPost("/books", new Book("1", "name1", "tester1"));
//
//        // when
//        List<Book> book = doGet("/books?byName=name2", new TypeReference<List<Book>>() {
//        });
//
//        // then
//        Assertions.assertEquals(0, book.size());
//    }
//
//    @Test
//    void shouldDeleteBook() {
//        // given
//        doPost("/books", new Book("1", "name1", "tester1"));
//        List<Book> originalBooks = bookRepository.findAll();
//
//        // when
//        doDelete("/books/1");
//        List<Book> bookAfterDelete = bookRepository.findAll();
//
//        // then
//        Assertions.assertEquals(1, originalBooks.size());
//        Assertions.assertEquals("name1", originalBooks.get(0).getName());
//        Assertions.assertEquals(0, bookAfterDelete.size());
//    }
//
//    @AfterEach
//    void clean() {
//        bookRepository.deleteAll();
//    }
}
