package pl.net.divo.course.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.net.divo.course.repository.BookRepository;

@RestController
public class BookController {

    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping("/books/isHealthy")
    public String isHealthy() {
        return "OK";
    }

    @DeleteMapping("/tests/{id}")
    public void remove(@PathVariable String id) {
        bookRepository.delete(id);
    }
}
