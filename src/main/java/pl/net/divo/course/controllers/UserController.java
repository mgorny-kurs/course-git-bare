package pl.net.divo.course.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.net.divo.course.model.Sample;
import pl.net.divo.course.model.User;

@RestController
public class UserController {


    @GetMapping("/users/isHealthy")
    public String isHealthy() {
        return "OK";
    }

    @GetMapping("/tests/{id}")
    public User getById(@PathVariable String id) {

        return userRepository.findById(id);
    }
}
