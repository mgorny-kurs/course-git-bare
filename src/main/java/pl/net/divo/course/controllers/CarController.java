package pl.net.divo.course.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.net.divo.course.model.Car;
import pl.net.divo.course.model.Sample;
import pl.net.divo.course.repository.CarRepository;
import pl.net.divo.course.repository.SampleRepository;

@RestController
@AllArgsConstructor
public class CarController {
    private final CarRepository carRepository;

    @PostMapping("/cars")
    public void add(@RequestBody Car car) {
        carRepository.save(car);
    }


    @GetMapping("/cars/isHealthy")
    public String isHealthy() {
        return "OK";
    }
}
