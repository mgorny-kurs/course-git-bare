package pl.net.divo.course.repository;

import org.springframework.stereotype.Repository;
import pl.net.divo.course.model.Book;

import java.util.*;
@Repository
public class BookRepository {
    private static final Map<String, Book> storage = new HashMap<>();

    public List<Book> findAll() {
        return new ArrayList<>(storage.values());
    }

    public void save(Book book) {
        storage.put(book.getId(), book);
    }

    public void delete(String id) {
        storage.remove(id);
    }

    public Book findById(String id) {
        return storage.get(id);
    }

    public Optional<Book> findByName(String name) {
        return storage.values().stream()
                .filter(sample -> sample.getName().equals(name))
                .findFirst();
    }

    public void deleteAll() {
        storage.clear();
    }
}


