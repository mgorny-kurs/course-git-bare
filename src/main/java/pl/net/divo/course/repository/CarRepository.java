package pl.net.divo.course.repository;

import pl.net.divo.course.model.Car;

import pl.net.divo.course.model.Sample;

import java.util.*;

public class CarRepository {

    private static final Map<String, Car> storage = new HashMap<>();

    public List<Car> findAll() {
        return new ArrayList<>(storage.values());
    }

    public Optional<Car> findByName(String name) {
        return storage.values().stream()
                .filter(car -> car.getName().equals(name))
                .findFirst();
    }

    public void delete(String id) {
        storage.remove(id);
    }

    public void save(Car car) {
        storage.put(car.getId(), car);
    }



}
