# Users
 - List users
 - Add new user
 - Delete user
 - Find user by id
 - Find user by name
 - Repair user test

# Books
 - List books
 - Add new book
 - Delete book
 - Find book by id
 - Find book by name
 - Repair book test

# Cars
 - List cars
 - Add new car
 - Delete car
 - Find car by id
 - Find car by name
 - Repair car test

# Photos
 - List photos
 - Add new photo
 - Delete photo
 - Find photo by id
 - Find photo by name
 - Repair photo test